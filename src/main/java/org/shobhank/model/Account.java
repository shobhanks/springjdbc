package org.shobhank.model;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 *
 * @author shsharma
 */
public class Account {
	
    @JsonProperty("accountId")
	String accountId;
    
    @JsonProperty("account")
	String account;
    
    @JsonProperty("status")
	String status;
	
	public String getAccountId() {
		return accountId;
	}
	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}
	public String getAccount() {
		return account;
	}
	public void setAccount(String account) {
		this.account = account;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("BasicContract{");
        sb.append("accountId='").append(accountId).append('\'');
        sb.append(", account='").append(account).append('\'');
        sb.append(", status='").append(status).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
