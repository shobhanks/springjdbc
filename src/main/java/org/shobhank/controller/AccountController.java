package org.shobhank.controller;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import org.shobhank.dao.AccountDAO;
import org.shobhank.model.Account;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;


/**
 *
 * @author shsharma
 */

@Controller
@Path("/v1/")
public class AccountController {
	@Autowired
	private AccountDAO accountDAO;
	
    @GET
    @Produces("application/json")
    @Path("/accounts")
	public Response getAccounts(){
		List<Account> accounts = accountDAO.list();
		return Response.ok(accounts).build();
	}
}
