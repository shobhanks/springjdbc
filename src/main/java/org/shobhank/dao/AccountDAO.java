package org.shobhank.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import javax.sql.DataSource;

import org.shobhank.model.Account;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

/**
 *
 * @author shsharma
 */

@Repository
public class AccountDAO {
    @Autowired
    private DataSource dataSource;
        
    public List<Account> list() {
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        String sql = "SELECT * from accounts where rownum<10";
        List<Account> listAccount = jdbcTemplate.query(sql, new RowMapper<Account>() {
            
            public Account mapRow(ResultSet rs, int rowNumber) throws SQLException {
                Account account = new Account();
                account.setAccountId(rs.getString("account_id"));
                account.setAccount(rs.getString("account"));
                account.setStatus(rs.getString("status"));
                return account;
            }
             
        });
        return listAccount;
    }
}
